# SainsburysChallenge


CHALLENGE REQUIREMENTS
----------------------

Using best practice coding methods, build an Android application that scrapes data
from a Sainsbury’s grocery page and displays the result in a list format. In addition to the
displayed list, a total value of all the products displayed should be shown at the top of the
screen.
The compiled APK must be compatible with Android 4.1 or later.

Data source link:
http://www.sainsburys.co.uk/webapp/wcs/stores/servlet/CategoryDisplay?listView=true&orderBy=FAVOURITES_FIRST&parent_category_rn=12518&top_category=12518&langId=44&beginIndex=0&pageSize=20&catalogId=10137&searchTerm=&categoryId=185749&listId=&storeId=10151&promotionId=#langId=44&storeId=10151&catalogId=10137&categoryId=185749&parent_category_rn=12518&top_category=12518&pageSize=20&orderBy=FAVOURITES_FIRST&searchTerm=&beginIndex=0&hideFilters=true

We are looking for
------------------
0. Demonstration of MVC principles at code level
0. Native code only (no WebViews)
0. Understanding of client-server design patterns
0. Attention to detail in the presentation of the data
0. Understanding of device and version compatibility


IMPLEMENTATION
--------------
![](https://github.com/abiemann/SainsburysChallenge/blob/master/app_video.gif)


FEATURE LIST
------------
0. Pull-To-Refresh list
0. Rotate phone and list remains at previous position
0. Customized color palette