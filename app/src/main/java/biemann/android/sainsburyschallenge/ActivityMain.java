package biemann.android.sainsburyschallenge;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import biemann.android.sainsburyschallenge.Adapters.AdapterProductsRecyclerView;

/**
 * Main Activity that's responsible for showing the list
 * There are no click events for a "detail" view, hence, there are no other Activities
 */
public class ActivityMain extends AppCompatActivity implements JSinterfaceScraper.ScraperCallback
{
    // Constants
    private final String TAG = getClass().getSimpleName();
    private final String KEY_RECYCLER_STATE = "recycler_state";
    private final String URL_SERVER_GET_REQUEST = "http://www.sainsburys.co.uk/webapp/wcs/stores/" +
            "servlet/CategoryDisplay?listView=true&orderBy=FAVOURITES_FIRST" +
            "&parent_category_rn=12518&top_category=12518&langId=44&beginIndex=0&pageSize=20" +
            "&catalogId=10137&searchTerm=&categoryId=185749&listId=&storeId=10151" +
            "&promotionId=%23langId=44&storeId=10151&catalogId=10137&categoryId=185749" +
            "&parent_category_rn=12518&top_category=12518&pageSize=20&orderBy=FAVOURITES_FIRST" +
            "&searchTerm=&beginIndex=0&hideFilters=true";

    // View references
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerView;
    private WebView mWebView;
    private LinearLayout mLayoutProductTotals;
    private TextView mTextViewProductCount;
    private TextView mTextViewProductPriceTotal;

    // Member Variables
    private AdapterProductsRecyclerView mAdapterProductsRecyclerView;
    private static ArrayList<DataModel> mListProducts;
    private Handler mHandler;
    private static Bundle mBundleRecyclerViewState;
    private static boolean mReceivedDataFromServer = false;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Toolbar config
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());

        // Product Totals
        mLayoutProductTotals = (LinearLayout) findViewById(R.id.product_totals_container);
        mTextViewProductCount = (TextView) findViewById(R.id.product_count_total);
        mTextViewProductPriceTotal = (TextView) findViewById(R.id.product_price_total);

        // Swipe to refresh
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.orange, R.color.green, R.color.blue);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener()
        {
            @Override
            public void onRefresh()
            {
                Log.d(TAG, "mSwipeRefreshLayout.onRefresh()");
                // Refresh product list
                requestDataFromServer();
            }
        });

        // Webview for javascript execution since the HTML needs to be decrypted
        mWebView = (WebView) findViewById(R.id.webview_decrypt);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.addJavascriptInterface(new JSinterfaceScraper(this), "injectedHtmlAccess");
        mWebView.setWebViewClient(new WebViewClient()
        {
            @Override
            public void onPageFinished(final WebView webview, String url)
            {
                super.onPageFinished(webview, url);

                webview.postDelayed(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        // return the decrypted HTML
                        webview.loadUrl("javascript:window.injectedHtmlAccess.retrieveHtml" +
                                "('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");
                    }
                }, 500);// a delay seems necessary to ensure that the data is ready reliably
            }
        });

        // RecyclerView initialization, and customization: horizontal divider bars
        mRecyclerView = (RecyclerView) findViewById(R.id.itemsRecyclerView);
        mRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(this));//do this before mRecyclerView.setAdapter()

        if (mListProducts == null)
        {
            mListProducts = new ArrayList<>();
        }
        mAdapterProductsRecyclerView = new AdapterProductsRecyclerView(ActivityMain.this, mListProducts);
        mRecyclerView.setAdapter(mAdapterProductsRecyclerView);

        // for UI thread operations
        mHandler = new Handler(Looper.getMainLooper());

        // automatically Refresh items when app is launched - but not on rotate !
        if (!mReceivedDataFromServer)
        {
            requestDataFromServer();
        } else
        {
            scrapedDataReady(mListProducts);
        }
    }

    @Override
    protected void onPause()
    {
        super.onPause();

        // save RecyclerView scroll position
        if (mReceivedDataFromServer)
        {
            mBundleRecyclerViewState = new Bundle();
            Parcelable listState = mRecyclerView.getLayoutManager().onSaveInstanceState();
            mBundleRecyclerViewState.putParcelable(KEY_RECYCLER_STATE, listState);
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        // return the mRecyclerView back to it's position before rotation took place
        if (mBundleRecyclerViewState != null)
        {
            Parcelable listState = mBundleRecyclerViewState.getParcelable(KEY_RECYCLER_STATE);
            mRecyclerView.getLayoutManager().onRestoreInstanceState(listState);
        }
    }

    void requestDataFromServer()
    {
        startRefreshAnimation();// show the animation on listview
        mWebView.loadUrl(URL_SERVER_GET_REQUEST);
    }

    private void startRefreshAnimation()
    {
        // In order for the animation to show, it must be called like this
        mSwipeRefreshLayout.post(new Runnable()
        {
            @Override
            public void run()
            {
                mSwipeRefreshLayout.setRefreshing(true);
            }
        });
    }

    private void stopRefreshAnimation()
    {
        mSwipeRefreshLayout.setRefreshing(false);
    }

    // ****
    // concrete implementation of JSinterfaceScraper.ScraperCallback
    // ****
    @Override
    public void scrapedDataReady(final ArrayList<DataModel> productList)
    {
        mHandler.post(new Runnable()
        {
            @Override
            public void run()
            {
                Log.d(TAG, "scrapedDataReady() called, productList size = "+ productList.size());
                mReceivedDataFromServer = true;
                stopRefreshAnimation();
                mListProducts = productList;

                // add up totals
                int iProductCount = mListProducts.size();
                float fTotalPrice = 0.0f;
                for(DataModel product : mListProducts)
                {
                    fTotalPrice += product.mPricePerUnit;
                }

                // show totals in the UI
                mLayoutProductTotals.setVisibility(View.VISIBLE);
                final String strCount = String.format(getString(R.string.product_total_count_showing), iProductCount);
                mTextViewProductCount.setText(strCount);
                final String strTotal = String.format(getString(R.string.product_price), fTotalPrice);
                mTextViewProductPriceTotal.setText(strTotal);

                // let adapter know that underlying data has changed
                mAdapterProductsRecyclerView.setNewData(mListProducts);
            }
        });
    }
}
