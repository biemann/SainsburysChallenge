package biemann.android.sainsburyschallenge;

import android.graphics.Bitmap;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * It's important to get notification of when a page has loaded - this client will do that for us
 */
public class WebViewClientScraper extends WebViewClient
{
    // Constants
    private final String TAG = getClass().getSimpleName();

    // Member Variables
    private WebViewClientCallbacks mWebViewClientCallbacks;

    public interface WebViewClientCallbacks
    {
        void onPageFinished(WebView view);
    }

    public WebViewClientScraper(WebViewClientCallbacks webViewClientCallbacks)
    {
        mWebViewClientCallbacks = webViewClientCallbacks;
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon)
    {
        super.onPageStarted(view, url, favicon);
        Log.d(TAG, "onPageStarted()");
    }

    @Override
    public void onPageFinished(WebView view, String url)
    {
        super.onPageFinished(view, url);
        Log.d(TAG, "onPageFinished()");
        mWebViewClientCallbacks.onPageFinished(view);
    }
}
