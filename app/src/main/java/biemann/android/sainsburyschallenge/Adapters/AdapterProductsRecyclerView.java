package biemann.android.sainsburyschallenge.Adapters;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import biemann.android.sainsburyschallenge.DataModel;
import biemann.android.sainsburyschallenge.R;

/**
 * Adapter to be used with ItemListActivity.mRecyclerView
 */
public class AdapterProductsRecyclerView extends RecyclerView.Adapter<ViewHolderProducts>
{
    private AppCompatActivity mAppCompatActivity;
    ArrayList<DataModel> mProductsList;

    // Constructor
    public AdapterProductsRecyclerView(AppCompatActivity compatActivity, ArrayList<DataModel> productsList)
    {
        mAppCompatActivity = compatActivity;
        mProductsList = productsList;
    }

    public void setNewData(ArrayList<DataModel> productsList)
    {
        mProductsList = productsList;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolderProducts onCreateViewHolder(ViewGroup viewGroup, int i)
    {
        View inflatedView = LayoutInflater.from(mAppCompatActivity).inflate(R.layout.row_product_item, viewGroup, false);
        return new ViewHolderProducts(inflatedView);
    }

    @Override
    public void onBindViewHolder(final ViewHolderProducts viewHolder, int position)
    {
        viewHolder.mTextViewProductName.setText(getItem(position).mProductName);
        viewHolder.mTextViewProductPrice.setText(String.format(mAppCompatActivity.getString(R.string.product_price), getItem(position).mPricePerUnit) );

        //NOTE: there is no click event required in the App specification, possible later feature
//        viewHolder.mTextViewQualificationName.setTag(position);//important in onClick()
//        viewHolder.mLinearLayoutContainer.setOnClickListener(new View.OnClickListener()
//        {
//            @Override
//            public void onClick(View v)
//            {
//                Context context = v.getContext();
//                Intent intent = new Intent(context, ActivityItemDetail.class);
//                intent.putExtra("ITEM_TAG", (Integer) viewHolder.mTextViewQualificationName.getTag());
//                context.startActivity(intent);
//            }
//        });
    }


    public DataModel getItem(int position)
    {
        return mProductsList.get(position);
    }

    @Override
    public int getItemCount()
    {
        return mProductsList.size();
    }

}
