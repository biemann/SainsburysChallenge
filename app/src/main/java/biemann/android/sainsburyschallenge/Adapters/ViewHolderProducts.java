package biemann.android.sainsburyschallenge.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import biemann.android.sainsburyschallenge.R;

/**
 * Typical Viewholder pattern, specifically for RecyclerView
 */
public class ViewHolderProducts extends RecyclerView.ViewHolder
{
    public LinearLayout mLinearLayoutContainer;
    public TextView mTextViewProductName;
    public TextView mTextViewProductPrice;

    public ViewHolderProducts(View itemView)
    {
        super(itemView);
        mLinearLayoutContainer = (LinearLayout) itemView.findViewById(R.id.layout_container);
        mTextViewProductName = (TextView) itemView.findViewById(R.id.product_name);
        mTextViewProductPrice = (TextView) itemView.findViewById(R.id.product_price);
    }

    @Override
    public String toString()
    {
        return super.toString() + " '" + mTextViewProductName.getText() + "'";
    }
}
