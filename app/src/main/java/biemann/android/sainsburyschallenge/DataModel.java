package biemann.android.sainsburyschallenge;

/**
 * The model of one grocery item
 */
public class DataModel
{
    public String mProductName;
    public String mProductImgUrl;
    public Float mPricePerUnit;
    public Float mPricePerMeasure;
    public Integer mUnitQuantity;
}
