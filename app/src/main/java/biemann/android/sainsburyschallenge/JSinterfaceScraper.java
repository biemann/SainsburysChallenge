package biemann.android.sainsburyschallenge;

import android.webkit.JavascriptInterface;

import java.util.ArrayList;

/**
 * Used to pull the decoded HTML from the DOM
 */
public class JSinterfaceScraper
{
    public interface ScraperCallback
    {
        void scrapedDataReady(ArrayList<DataModel> productList);
    }

    // Constants
    private final String TOKEN_PRODUCT_START = "productName\">";
    private final String TOKEN_PRODUCT_TITLE_START = ">";
    private final String TOKEN_PRODUCT_TITLE_END = "<";
    private final String TOKEN_PRODUCT_PRICE_START = "pricePerUnit\">";
    private final String TOKEN_PRODUCT_PRICE_END = "<";

    private ScraperCallback mScraperCallback;

    public JSinterfaceScraper(ScraperCallback callback)
    {
        mScraperCallback = callback;
    }

    @JavascriptInterface
    public void retrieveHtml(String html)
    {
        ArrayList<DataModel> productList = new ArrayList<>();
        int index = 0;

        // Cycle through the HTML whilst we're finding the string tags
        while(true)
        {
            int iPosProdStart = html.indexOf(TOKEN_PRODUCT_START, index);
            if (iPosProdStart == -1) break;//Exit loop if nothing more found
            index = iPosProdStart + TOKEN_PRODUCT_START.length();

            int iPosProdTitleStart = html.indexOf(TOKEN_PRODUCT_TITLE_START, index);
            iPosProdTitleStart += TOKEN_PRODUCT_TITLE_START.length();
            index = iPosProdTitleStart;//iPosProdTitleStart + TOKEN_PRODUCT_TITLE_START.length();

            int iPosProdTitleEnd = html.indexOf(TOKEN_PRODUCT_TITLE_END, index);
            index = iPosProdTitleEnd + TOKEN_PRODUCT_TITLE_END.length();

            DataModel product = new DataModel();

            // Extract product Name
            String strTitleDirty = html.substring(iPosProdTitleStart, iPosProdTitleEnd);
            product.mProductName = cleanTitle(strTitleDirty);


            int iPosPriceStart = html.indexOf(TOKEN_PRODUCT_PRICE_START, index);
            iPosPriceStart += TOKEN_PRODUCT_PRICE_START.length();
            index = iPosPriceStart;

            int iPosPriceEnd = html.indexOf(TOKEN_PRODUCT_PRICE_END, index);
            index = iPosPriceEnd + TOKEN_PRODUCT_PRICE_END.length();

            // Extract product Price
            String strPriceDirty = html.substring(iPosPriceStart, iPosPriceEnd);
            product.mPricePerUnit = cleanDirtyPrice(strPriceDirty);


            productList.add(product);
        }


        if (mScraperCallback != null && productList.size() > 0)
        {
            mScraperCallback.scrapedDataReady(productList);
        }
    }

    private String cleanTitle(String strDirtyTitle)
    {
        // replace newline and tab by a space
        String cleaner = strDirtyTitle.replace('\n',' ');
        cleaner = cleaner.replace('\t',' ');

        // replace multiple spaces
        cleaner = cleaner.trim().replaceAll(" +", " ");

        // replace &amp
        cleaner = cleaner.replace("&amp;", "&");

        return cleaner;
    }

    private Float cleanDirtyPrice(String strDirtyPrice)
    {
        // replace newline by a space
        String cleaner = strDirtyPrice.replace('\n',' ');

        // replace Pound Symbol by a space
        cleaner = cleaner.replace('£',' ');

        return Float.valueOf(cleaner);// leading spaces have no effect on the conversion
    }
}
